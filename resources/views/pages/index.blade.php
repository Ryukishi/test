
@extends('layouts.app')   

@section('content')
<div class="index">
   <div class="title m-b-md">
        {{$title}}
    </div>
    <div class="about">
        This is an application that works with the Todo - Task Manager 
        product. It is a task manager for the youth to keep up with the tasks 
        of their busy day to day life.
    </div>
</div>
@endsection