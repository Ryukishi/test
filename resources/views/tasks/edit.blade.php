@extends('layouts.app')

@section('content')
    <div class="title m-b-md">
        Edit Task
    </div>

    {!! Form::open(['action' => ['TasksController@update', $task->id], 'method' => 'POST']) !!}
    {{Form::macro('myField', function()
    {
    return '<input type="time">';
    })}}
    <div class="input-field">
        <div class="formgroup">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $task->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
    </div>
    <div class="input-field">
        <div class="formgroup">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', $task->description, ['class' => 'form-control', 'placeholder' => 'Description'])}}
        </div>
    </div>
    <div class="input-field">
        <div class="formgroup">
            {{Form::label('duration', 'Duration')}}
            {{Form::number('duration', $task->duration, ['class' => 'form-control', 'placeholder' => 'Duration'])}}
        </div>
    </div>
    <div class="input-field">
        <div class="formgroup">
            {{Form::label('time', 'Time')}}
            {{Form::time('time', $task->time, ['class' => 'form-control', 'placeholder' => 'Time'])}}
        </div>
    </div>
    <a href="/tasks/{{$task->id}}" class="btn btn-primary">Go Back</a>

    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
    <div class="container">
    </div>
@endsection
