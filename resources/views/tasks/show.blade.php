@extends('layouts.app')

@section('content')
    <div class="title m-b-md">
        Task {{$task->name}}
    </div>
    <div class="container">
        <h3>Description: {{$task->description}}</h3>
        @if($task->duration > 1)
            <h3>Duration: {{$task->duration}} hours</h3>
        @else
            <h3>Duration: {{$task->duration}} hour</h3>
        @endif
        <h3>Time: {{$task->time}}</h3>

        {!! Form::open(['action' => ['TasksController@destroy', $task->id], 'method' => 'POST', 'class' => 'pull-right']) !!}
        <a href="/tasks" class="btn btn-primary">Go Back</a>
        <a href="/tasks/{{$task->id}}/edit" class="btn btn-primary">Edit Task</a>
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
    </div>
@endsection
