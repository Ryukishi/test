@extends('layouts.app')

@section('content')
    <div class="title m-b-md">
        Task overview
    </div>
    <div class="container">
        @if(count($tasks) > 0)
            @foreach($tasks as $task)
                <div class="task">
                <h3><a href="/tasks/{{$task->id}}">{{$task->name}}</a></h3>
                    Description: {{$task->description}}<br>
                <small>Occupancy: {{$task->duration}} hours&emsp;&emsp;</small>
                <small>Time: {{$task->time}}</small>
                </div>
                <br>
            @endforeach
        @else
            <p> No tasks found</p>
        @endif
    </div>
@endsection
