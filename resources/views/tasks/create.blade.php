@extends('layouts.app')

@section('content')
    <div class="title m-b-md">
        {{$title}}
    </div>
    {!! Form::open(['action' => 'TasksController@store', 'method' => 'POST']) !!}
    <div class="input-field">
        <div class="formgroup">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
    </div>
        <div class="input-field">
        <div class="formgroup">
                {{Form::label('description', 'Description')}}
                {{Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'Description'])}}
        </div>
    </div>
        <div class="input-field">
        <div class="formgroup">
            {{Form::label('duration', 'Duration')}}
            {{Form::text('duration', '', ['class' => 'form-control', 'placeholder' => 'Duration'])}}
        </div>
    </div>
        <div class="input-field">
        <div class="formgroup">
                {{Form::label('time', 'Time')}}
                {{Form::text('time', '', ['class' => 'form-control', 'placeholder' => 'Time'])}}
        </div>
    </div>
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
