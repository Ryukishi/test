<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <head>
    
        <title>{{config('app.name', 'Todo - Task Manager')}}</title>

    </head>
    <body>
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
            @include('inc.navbar')
            @include('inc.messages')
            @yield('content')
        </div>
    </body>
</html>
