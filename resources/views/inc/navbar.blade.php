{{-- Navigation bar --}}

<header class="mb-auto">
    <div class="inner">
        <h3 class="masthead-brand">Todo</h3>
        <nav class="nav nav-masthead justify-content-center">
        <a class="nav-link" href="/">Home</a>
        <a class="nav-link" href="/create">Create</a>
        <a class="nav-link" href="/overview">Overview</a>
        </nav>
    </div>
</header>
