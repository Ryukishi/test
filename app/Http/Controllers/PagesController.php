<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    // Homepage
    public function index(){
        $title = 'Welcome to Todo';
        return view('pages.index')->with('title', $title);
    }

    // Overview page
    public function overview(){
        return view('tasks.overview');
    }

    // Dev info page
    public function phpinfp(){
        return view('pages.phpinfo');
    }
}
