FROM php:7.2-fpm
#RUN docker-php-ext-install mysqli pdo pdo_mysql

# Get mysql drivers
RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql