<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Sends incoming get requests the landing page
Route::get('/', 'PagesController@index');

// Sends incoming get requests to task overview page
Route::get('/overview', 'TasksController@index');

// Sends incoming get requests to create page
Route::get('/create', 'TasksController@create');

// Sends incoming get requests to dev info page
Route::get('/info', 'PagesController@phpinfo');


Route::resource('tasks', 'TasksController');

